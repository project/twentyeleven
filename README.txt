INTRODUCTION
------------
TwentyEleven is a Drupal theme inspired by Wordpress' TwentyEleven theme
(http://wordpress.org/themes/twentyeleven).
It has the following features:
 * Front page with choice of One column/ Two column layout.
 * Drop down menus.
 * Configurable sidebar position (left or right).
 * Fully responsive theme.
 * Footer with 3 block regions.
 * Masthead images. A number of sample images are provided. Custom
   images can be uploaded.
INSTALLATION
------------
Install as you would normally install a contributed drupal theme.
See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.
CONFIGURATION
-------------
To achieve the same desired look and feel as Wordpress's TwentyEleven,
it is preferred to place the Search form in the Search region.
To configure the position of the serach block, visit
/admin/structure/block and use the pull-down menu to place the Search
form in the Search region.
The following configuration items are available for TwentyEleven. They
can be configured by visiting admin/appearance/settings/twentyeleven.
* Header Images: They must be one of jpg, bmp, gif or png formats and
  1000 x 288 pixels.  Users can choose to display a random header
  image or a specific fixed header image.
* Sidebar Position: can be chosen to be placed either left or right.
MAINTAINERS
-----------
* Lakshmi Narasimhan (lakshminp) - https://drupal.org/user/1022778
* Thomas Lattimore (tlattimore) - https://drupal.org/user/624754
